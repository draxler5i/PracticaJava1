package Excercises;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Daniel Merida
 */

public class Exercise47 {
    public static void main(String[]args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        buscarConsecutivos(n);
    }

    private static void buscarConsecutivos(int n) {
        n = 2;
        ArrayList<Integer> aux1 = new ArrayList<>();
        ArrayList<Integer> aux2 = new ArrayList<>();
        int i = 10;
        boolean encontrados = false;
        while(!encontrados) {
            aux1 = getPrimos(i);
            aux2 = getPrimos(i+1);
            if (aux1.size() == n && aux2.size() == n) {
                if (primosDistintos(aux1, aux2)) {
                    System.out.println(i+" = "+aux1.toString());
                    System.out.println((i+1)+" = "+aux2.toString());
                    encontrados = true;
                }
            }
            i = i+2;
        }
    }
    
    public static ArrayList<Integer> getPrimos(int n) {
        ArrayList<Integer> res = new ArrayList<>();
        
        for(int i = 2 ; i <= n ; i++){
            
            while( n % i == 0){
                n = n / i;
                res.add(i);
                 if(n == 1){
                    break;
                }
            }
        }
        return res;
    }
    
    public static boolean primosDistintos(ArrayList<Integer> lista1, ArrayList<Integer> lista2) {
        boolean res = true;
        //en el metodo principal ya esta comprobado que tienen el mismo tamaño
        for (int i = 0; i < lista1.size(); i++) {
            if (lista1.get(i) == lista2.get(i)) {
                res = false;
                return res;
            }
        }
        return res;
    }
    
    /***
     * Generico
     ***/
    /*
    private static void buscarConsecutivosN(int n) {
        //n = 2;
        ArrayList<ArrayList<Integer>> consecutivos = new ArrayList<>();
        
        //ArrayList<Integer> aux1 = new ArrayList<>();
        //ArrayList<Integer> aux2 = new ArrayList<>();
        
        int i = 10;
        boolean encontrados = false;
        boolean tamaños = false;
        while(!encontrados) {
            for (int j = 0; j < consecutivos.size(); j++) {
                consecutivos.add(getPrimos(i));
            }
            //aux1 = getPrimos(i);
            //aux2 = getPrimos(i+1);
            
            
            //compara los tamaños
            for (int j = 1; j < consecutivos.size(); j++) {
                if (consecutivos.get(0).size() == consecutivos.get(j).size()) {
                    tamaños = true;
                } else {
                    tamaños = false;
                    break;
                }
            }
            
            for (int j = 0; j < consecutivos.size(); j++) {
                if (j == consecutivos.size()-1) {
                    if (!primosDistintos(consecutivos.get(0), consecutivos.get(j))) {
                        
                    }
                }
            }
            
            
            if (aux1.size() == n && aux2.size() == n) {
                if (primosDistintos(aux1, aux2)) {
                    System.out.println(i+" = "+aux1.toString());
                    System.out.println((i+1)+" = "+aux2.toString());
                    encontrados = true;
                }
            }
            i = i+2; ///
        }
    }
    
    public static boolean primosDistintos2(ArrayList<Integer> lista1, ArrayList<Integer> lista2) {
        boolean res = true;
        //en el metodo principal ya esta comprobado que tienen el mismo tamaño
        for (int i = 0; i < lista1.size(); i++) {
            if (lista1.get(i) == lista2.get(i)) {
                res = false;
                return res;
            }
        }
        return res;
    }

    */
}
