package Excercises;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/**
 *
 * @author Daniel Merida
 */

public class Exercise1025B {
    public static void main(String []args) throws IOException{
        
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	PrintWriter output = new PrintWriter(new OutputStreamWriter(System.out));
	String cadena;
        
        int n = Integer.parseInt(input.readLine());
        int auxN = n;
        ArrayList<Integer> multPrimsA = new ArrayList<>();
        ArrayList<Integer> multPrimsB = new ArrayList<>();
        
	while(n > 0) {		   
            cadena = input.readLine();    
            StringTokenizer st = new StringTokenizer(cadena);
            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());
            multPrimsA.add(a);
            multPrimsB.add(b);
            n--;
        }
        
        
        for (Integer entero : multPrimsA) {
            System.out.println("Los divisores de "+entero+" son: "+ getPrimos(entero).toString());
        }
        for (Integer entero : multPrimsB) {
            System.out.println("Los divisores de "+entero+" son: "+ getPrimos(entero).toString());
        }
        boolean flag = false;
        int res = -1;
        if (auxN == 1) {
            System.out.println("Respuesta "+getPrimos(multPrimsA.get(0)).get(0));
        } else {
            
            for (int i = 0; i < multPrimsA.size(); i++) {
                
                ArrayList<Integer> auxPrimsA = getPrimos(multPrimsA.get(i));

                if (i < multPrimsA.size() - 1) {

                    if (existeEn(auxPrimsA.get(i), getPrimos(multPrimsA.get(i+1)))) {
                        res = auxPrimsA.get(i);
                        break;
                    } else {
                        if (existeEn(auxPrimsA.get(i), getPrimos(multPrimsB.get(i+1)))) {
                            res = auxPrimsA.get(i);
                            break;
                        }
                    }
                }
                if (res != -1) {
                    flag = true;
                }
            }
            
            if (!flag) {
                for (int i = 0; i < multPrimsB.size(); i++) {

                    ArrayList<Integer> auxPrimsB = getPrimos(multPrimsA.get(i));

                    if (i < multPrimsA.size() - 1) {

                        if (existeEn(auxPrimsB.get(i), getPrimos(multPrimsA.get(i+1)))) {
                            res = auxPrimsB.get(i);
                            break;
                        } else {
                            if (existeEn(auxPrimsB.get(i), getPrimos(multPrimsB.get(i+1)))) {
                                res = auxPrimsB.get(i);
                                break;
                            }
                        }
                    }        
                }
            }
            System.out.println("La respuesta es "+res);    
        }
        output.flush();
    }
    
    public static ArrayList<Integer> getPrimos(int n) {
        
        ArrayList<Integer> res = new ArrayList<>();
        Set<Integer> hs = new HashSet<>();
        
        for(int i = 2 ; i <= n ; i++){
            
            while( n % i == 0){
                n = n / i;
                //res.add(i);
                hs.add(i);
                 if(n == 1){
                    break;
                }
            }
        }
        //Eliminar los #'s repetidos
        hs.addAll(res);
        res.clear();
        res.addAll(hs);
        return res;
    }
    
    public static boolean existeEn(int numero, ArrayList<Integer> lista) {
        boolean res = false;
        if (lista.contains(numero)) {
            res = true;
        }
        return res;
    }
}