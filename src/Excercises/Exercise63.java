package Excercises;

/**
 *
 * @author Daniel Merida
 */
public class Exercise63 {
    
    private int lastNumber;
    private int exponent;
    
    public void findPowerfullDigis() {
        int numero = 16800;
        //tiene como rango [16800, 134217729]
        while(numero < 134217729) {
            exponent = countDigits(numero);
            lastNumber = numero%10;
            if (Math.pow(lastNumber, exponent) == numero ) {
                System.out.println(numero+" = "+lastNumber+" e "+exponent);
            }
            numero ++;
        }
    }
    
    private int countDigits(int dig) {
        return (int) (Math.log10(dig)+1);
    }
}
