package Excercises;

/**
 *
 * @author Daniel Merida
 */
public class TimeInWords {
    private int hour;
    private int minutes;
    public TimeInWords(int hour, int minutes) {
        this.hour = hour;
        this.minutes = minutes;
        formHour(hour, minutes);
    }
    
    private void formHour(int h, int m) {
        String hour = getLiteralH(h);
        String minutes = getLiteralM(m);
        String res = "";
        if (h < 13 && h > 0 && m >= 00 && m <= 60){
            if (m == 0) {
                res = hour + minutes;
            } else if ( m > 0 && m <= 30) {
                res = minutes + hour;
            } else {
                hour = getLiteralH(h+1);
                res = minutes + hour;
            }
        }
        System.out.println(res);
    }

    private String getLiteralM(int m) {
        String res = "";
        switch(m){
            case 00:
                res = " o' clock";
                break;
            case 01:
                res = "one minute past ";
                break;
            case 10:
                res = "ten minutes past ";
                break;
            case 15:
                res = "quarter past ";
                break;
            case 30:
                res = "half past ";
                break;
            case 40:
                res = "twenty minutes to ";
                break;
            case 45:
                res = "quarter to ";
                break;
            case 47:
                res = "thirteen minutes to ";
                break;
            case 28:
                res = "twenty eight minutes past ";
                break;
        }
        return res;
    }

    private String getLiteralH(int h) {
        String res = "";
        switch(h){
            case 1:
                res = "one";
                break;
            case 2:
                res = "two";
                break;
            case 3:
                res = "three";
                break;
            case 4:
                res = "four";
                break;
            case 5:
                res = "five";
                break;
            case 6:
                res = "six";
                break;
            case 7:
                res = "seven";
                break;
            case 8:
                res = "eight";
                break;
            case 9:
                res = "nine";
                break;
            case 10:
                res = "ten";
                break;
            case 11:
                res = "eleven";
                break;
            case 12:
                res = "twelve";
                break;
        }
        return res;
    }
}
